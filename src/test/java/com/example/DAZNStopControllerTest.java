package com.example;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DAZNStopControllerTest {
	@LocalServerPort
	private int port;
	
	private URL base;
	
	@Autowired
	private TestRestTemplate template;
	
	@Before
	public void setUp() throws Exception {
		this.base = new URL("http://localhost:" + port + "/");
	}
	
	@Test
	public void startsAndStopsWiremockServer() throws Exception {
		ResponseEntity<String> response = template.getForEntity(base.toString() + "start", String.class);
		
		assertThat(response.getStatusCode().value(), equalTo(200));
		
		response = template.getForEntity(base.toString() + "stop?port=" + Integer.parseInt(response.getBody()), String.class);
		
		assertThat(response.getStatusCode().value(), equalTo(200));
		assertThat(response.getBody(), equalTo(null));

	}
	
	@Test
	public void doesntStopNotStartedWiremockServer() throws Exception {
		ResponseEntity<String> response = template.getForEntity(base.toString() + "stop?port=7999", String.class);
		
		assertThat(response.getStatusCode().value(), equalTo(200));
		assertThat(response.getBody(), equalTo("not found"));
	}
}
