package com.example;

import java.util.LinkedList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.github.tomakehurst.wiremock.WireMockServer;

@SpringBootApplication
public class DaznWiremockServerApplication {

	public static List<WireMockServer> wiremockInstances = new LinkedList();
	
	public static void main(String[] args) {
		SpringApplication.run(DaznWiremockServerApplication.class, args);
	}
	
	@Override
	protected void finalize() throws Throwable {
		
		for (WireMockServer server : wiremockInstances ) {
			server.stop();
		}
		super.finalize();
	}
}
