package com.example;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

import java.util.Random;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.common.SingleRootFileSource;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.standalone.JsonFileMappingsSource;
import com.github.tomakehurst.wiremock.standalone.MappingsLoader;

@RestController
@RequestMapping("/record")
public class DAZNRecordController {
	
	private static Random random = new Random();

	public static class URL {
		private String url;

		public URL() {}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}
		
		
		
	}
	@RequestMapping(method = RequestMethod.GET)
	String start() {
		int port = 8000 + random.nextInt(999);
		WireMockServer wireMockServer = new WireMockServer(new WireMockConfiguration().port(port).proxyVia("http://date.jsontest.com", 80));
		wireMockServer.start();
		
		DaznWiremockServerApplication.wiremockInstances.add(wireMockServer);
		
		return "" + port;
	}

	@RequestMapping(path="start", method = RequestMethod.PUT)
	String startWithProfile(@RequestParam(value="profile", required = true, defaultValue="anonymous") String profileName, @RequestBody URL url) {
		FileSource fileSource =  new SingleRootFileSource("profiles/" + profileName);
		fileSource.createIfNecessary();
		int port = 8000 + random.nextInt(999);
		FileSource filesFileSource = new SingleRootFileSource("profiles/"+profileName+"/__files");
		FileSource mappingsFileSource = new SingleRootFileSource("profiles/"+profileName+"/mappings");
		filesFileSource.createIfNecessary();
		mappingsFileSource.createIfNecessary();
		WireMockServer wireMockServer = new WireMockServer(new WireMockConfiguration().port(port).fileSource(fileSource));
//		wireMockServer.loadMappingsUsing(new JsonFileMappingsSource(filesFileSource));
		wireMockServer.enableRecordMappings(mappingsFileSource, filesFileSource);
		wireMockServer.stubFor(any(urlMatching(".*")).willReturn(aResponse().proxiedFrom(url.url)));
		wireMockServer.start();
		
		DaznWiremockServerApplication.wiremockInstances.add(wireMockServer);
		
		return "" + port;
	}

}
