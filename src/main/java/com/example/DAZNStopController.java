package com.example;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.tomakehurst.wiremock.WireMockServer;

@RestController
@RequestMapping("/stop")
public class DAZNStopController {

	@RequestMapping(method = RequestMethod.GET)
	String stopOnPort(@RequestParam(value="port", required = true, defaultValue="-1") int port) {
		int index = 0;
		int foundIndex = -1;
		for (WireMockServer server : DaznWiremockServerApplication.wiremockInstances) {
			if (server.port() == port) {
				foundIndex = index;
				server.stop();
			}
			index++;
		}

		if (foundIndex != -1) {
			DaznWiremockServerApplication.wiremockInstances.remove(foundIndex);
			return "";
		}
		
		return "not found";
	}

}
