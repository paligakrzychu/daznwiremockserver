package com.example;

import java.net.URI;
import java.util.Random;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.common.SingleRootFileSource;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.standalone.JsonFileMappingsSource;


@RestController
@RequestMapping("/start")
public class DAZNStartController {

	private static Random random = new Random();

	@RequestMapping(method = RequestMethod.GET)
	String start() {
		int port = 8000 + random.nextInt(999);
		WireMockServer wireMockServer = new WireMockServer(new WireMockConfiguration().port(port));
		wireMockServer.start();
		
		DaznWiremockServerApplication.wiremockInstances.add(wireMockServer);
		
		return "" + port;
	}

	@RequestMapping(value = "profile", method = RequestMethod.GET)
	String startWithProfile(@RequestParam(value="name", required = true, defaultValue="anonymous") String profileName) {
		FileSource fileSource =  new SingleRootFileSource("profiles/" + profileName);
		FileSource filesFileSource = new SingleRootFileSource("profiles/"+profileName+"/__files");
		FileSource mappingsFileSource = new SingleRootFileSource("profiles/"+profileName+"/mappings");				
		fileSource.createIfNecessary();
		filesFileSource.createIfNecessary();
		mappingsFileSource.createIfNecessary();
		int port = 8000 + random.nextInt(999);
		WireMockServer wireMockServer = new WireMockServer(new WireMockConfiguration().port(port).fileSource(fileSource));
		wireMockServer.loadMappingsUsing(new JsonFileMappingsSource(mappingsFileSource));
		wireMockServer.start();
		wireMockServer.loadMappingsUsing(new JsonFileMappingsSource(mappingsFileSource));

		
		DaznWiremockServerApplication.wiremockInstances.add(wireMockServer);
		
		return "" + port;
	}
	
	
	
}
